README.txt

This app needs a simple file server running. The easiest thing to do is use http-server.

See: https://github.com/indexzero/http-server

Install:
$ npm install jitsu -g
....then in the root dir of the app...
$ jitsu install http-server

Then run:
$ http-server -a localhost -p 8080

Point browser to:
http://localhost:8080/venn-diagram.html
