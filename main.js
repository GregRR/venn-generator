// SVG methods
SVG = {
  createContainer : function( width, height, containerId, svgId ){
    var container = document.getElementById( containerId );
    var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svg.setAttribute('width', width);
    svg.setAttribute('height', height);
    svg.setAttribute('id', svgId);
    container.appendChild( svg );    
  },
  createCircle : function (cx, cy, r, rgbaColor, svgId, circleId) {
    var svgId = document.getElementById( svgId );
    var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    circle.setAttribute("cx", cx);
		circle.setAttribute("cy", cy);
		circle.setAttribute("r", r);
		circle.setAttribute("style","fill: " + rgbaColor + ";");
		circle.setAttribute('id', circleId);
		svgId.appendChild( circle );
  }
}

// find intersection length of two arrays
function countIntersection(array1, array2) {
	var arrays = [ array1, array2 ];
	var intersectionArray = arrays.shift().reduce(function(res, v) {
    if (res.indexOf(v) === -1 && arrays.every(function(a) {
        return a.indexOf(v) !== -1;
    })) res.push(v);
    return res;
	}, []);
	var intersection = intersectionArray.length;
	return intersection;
}

//create table of data
//not very elegant, I know - but it's a small, static table :)
function createTable(set1, set2, intersection) {
	// if previous one exists, clear it first
	if (document.getElementById("venn-data") != null) {
		$( "#venn-data" ).remove();
	}
	$("#venn-table").append(
		"<table id='venn-data' class='table' style='width: 20%'><tbody>" +
		"<tr><td>Set A</td><td>" + set1 + "</td></tr>" +
		"<tr><td>Set B</td><td>"+ set2 +"</td></tr>" +
		"<tr><td>AB Intersect</td><td>" + intersection + "</td></tr>" +
		"</tbody></table>"
	);
}

// Larger circle r always 100px, find relative r in px for second circle 
function calcSecondRadius(t1, t2) {
	var radius = (Math.min(t1, t2) * 100) / Math.max(t1, t2);
	var pxRadius = (Math.round(radius));
	console.log("This: " + pxRadius);
	return pxRadius;
}

function estOverlapArea(radius, setLength, interaction) {
	return 	interaction * (2 * radius) / setLength;
}

function calcOriginDistance(set1Length, radius1, set2Length, radius2, intersection) {
	//find expected overlap area
	/* for the REAL method....not using for now
	var a1 = Math.round(Math.PI * radius1 * radius1);
	console.log("Area of circle1: " + a1);
	var intA = Math.round(intersection * a1 / set1Length);
	console.log("Intersection area: " + intA);
	*/
	
	// using very simplified estimate of overlap for now
	// my own home-brewed estimate method
	var d; //distance (x) from origin to origin
	var areaEst1 = Math.round(estOverlapArea(radius1, set1Length, intersection));
	var areaEst2 = Math.round(estOverlapArea(radius2, set2Length, intersection));
	var areaOverlap = Math.round((areaEst1 + areaEst2) / 2);
	d = Math.round(radius1 + radius2 - areaOverlap - (areaOverlap * .1));
  
	return d;
}

//create venn diagram and table
function createVenn() {
	// if previous one exists, clear it
	if (document.getElementById("venn1") != null) {
		$( "#venn1" ).remove();
	}
	var data = $.getJSON( "/data/venn.json", function( data ) {
		var set1Length = Object.keys(data[0].genes).length;
		var set2Length = Object.keys(data[1].genes).length;
		var geneSet1 = [];
		var geneSet2 = [];
		geneSet1 = data[0].genes;
		geneSet2 = data[1].genes;
		var intersection = countIntersection(geneSet1, geneSet2);
		var c1Radius = 100; // larger set gets set to 100px radius
		var c2Radius = calcSecondRadius(set1Length, set2Length);
		
		var d = calcOriginDistance(set1Length, c1Radius, set2Length, c2Radius, intersection);
		
		var xOriginC1 = 150;
		var xOriginC2 = xOriginC1 + d;
	 
		SVG.createContainer( "100%", "300px", "venn", "venn1");
		SVG.createCircle(xOriginC1, "150px", c1Radius, "rgba(30,255,20,0.6)", "venn1", "circle1");
		SVG.createCircle(xOriginC2, "150px", c2Radius, "rgba(255,50,20,0.6)", "venn1", "circle2");
		
		// add table
		createTable(set1Length, set2Length, intersection);	
	});	
}

//button listener
$(function(){
    $("#generate").click(function(){
       	createVenn(); 
    });
});
